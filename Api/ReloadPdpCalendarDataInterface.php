<?php
declare(strict_types=1);

namespace Janis\JanisConnector\Api;

interface ReloadPdpCalendarDataInterface
{
    /**
     * @param $var
     * @return mixed
     */
    public function execute();
}
